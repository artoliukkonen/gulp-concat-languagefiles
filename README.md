# gulp-concat-languagefiles

> Gulp plugin to combine several nested JSON language files

## Usage

Install `gulp-concat-languagefiles` as a development dependency:

```shell
npm install --save-dev gulp-concat-languagefiles
```

Add it to your `gulpfile.js`:

```javascript
var concat_languagefiles = require("gulp-concat-languagefiles");

gulp.src("src/app/**/languages/*.json")
	.pipe(concat_languagefiles("translations.js"))
	.pipe(gulp.dest(""));
```

You can also use gulp-convert to convert result to CSV

```javascript
var concat_json = require("gulp-concat-languagefiles");
var convert = require("gulp-convert");

gulp.src("src/app/**/languages/*.json")
	.pipe(concat_languagefiles("project.js"))
	.pipe(convert({from:'json', to: 'csv'}))
	.pipe(gulp.dest(""));
```

See /example for a script to parse generated CSV back to original JSON-files, it even supports downloading CSV from Google Drive!
